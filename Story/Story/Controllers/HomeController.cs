﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Story.Models;

namespace Story.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

      
        public ActionResult Stories()
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../story.xml"));
            XmlNode xNode = XmlDocObj.SelectSingleNode("stories");
            XmlNodeList story = xNode.ChildNodes;

            

            return View(story);

        }
        public ActionResult Add(Story.Models.Story s)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../story.xml"));
            XmlNode xNode = XmlDoc.SelectSingleNode("stories");

            int Number = xNode.ChildNodes.Count;
            XmlNode xmlNode = xNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "story", ""));

            xmlNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Id", "")).InnerText = Number.ToString();
            xmlNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "story", "")).InnerText = s.story;
          

            XmlDoc.Save(Server.MapPath("../story.xml"));


            return RedirectToAction("Index");

        }
        public ActionResult Edit(String id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../story.xml"));
            XmlNode xNode = XmlDoc.SelectSingleNode("stories");
            XmlNodeList stories = xNode.ChildNodes;

            XmlNode st = stories[0];

            foreach (XmlNode s in stories)
            {
                if (s["Id"].InnerText == id)
                {
                    st = s;
                }
            }

            return View(st);

        }
        public ActionResult story(String id) 
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../story.xml"));
            XmlNode xNode = XmlDoc.SelectSingleNode("stories");
            XmlNodeList stories = xNode.ChildNodes;

            XmlNode st = stories[0];

            foreach (XmlNode s in stories)
            {
                if (s["Id"].InnerText == id)
                {
                    st = s;
                }
            }

            return View(st);

        }
        public ActionResult Update(Story.Models.Story story2) 
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../story.xml"));
            XmlNode xNode = XmlDocObj.SelectSingleNode("stories");
            XmlNodeList stories = xNode.ChildNodes;

            XmlNode st = stories[0];

            foreach (XmlNode s in stories)
            {
                if (s["Id"].InnerText == story2.id.ToString())
                {
                    st = s;
                }
            }

            st["story"].InnerText = story2.story;
           


            XmlDocObj.Save(Server.MapPath("../story.xml"));

            return RedirectToAction("Stories");

        }

        public ActionResult Remove(String id)
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../../story.xml"));
            XmlNode xNode = XmlDocObj.SelectSingleNode("stories");
            XmlNodeList story = xNode.ChildNodes;

            XmlNode s = story[0];

            foreach (XmlNode st in story)
            {
                if (st["Id"].InnerText == id)
                {
                    s = st;
                }
            }

            xNode.RemoveChild(s);

            XmlDocObj.Save(Server.MapPath("../../story.xml"));

            return RedirectToAction("Stories");
        }
    }
}